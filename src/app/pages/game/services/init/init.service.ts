
// import common service

import { Observable } from 'rxjs';
import { MainService } from 'src/app/infrastructure/api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.localStorage.getItem('userId');

  getAllSotedScoresWihUser(): Observable<any> {
    return this._apiService.get(`user/best/scores`);
  }

  getUsersBestScore(): Observable<any> {
    return this._apiService.get(`api/feedback/getOne/${this.loggedUser}`);
  }
  

  updateUserScore(obj): Observable<any> {
    return this._apiService.post(`user/update/score`, obj);
  }
  postObjNearByPlaceMapRequest(obj): Observable<any> {
    return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  }

  validatePoint(obj): Observable<any> {
    return this._apiService.post(`googleApi/validatePoint`, obj);
  }

  putObj(obj): Observable<any> {
    return this._apiService.patch(
      `api/feedback//update/${this.loggedUser}`,
      obj
    );
  }

  deleteObj(id): Observable<any> {
    return this._apiService.delete(`api/feedback/remove/${id}`);
  }
}
