import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { GamePage } from './game.page';
import { BestScoreManager } from './app.storage.service';
import { InitService } from './services/init/init.service';

const routes: Routes = [
  {
    path: '',
    component: GamePage
  }
];

@NgModule({
  imports: [
    IonicSwipeAllModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    InitService,
    BestScoreManager
  ],
  declarations: [GamePage]
})
export class GamePageModule {}
