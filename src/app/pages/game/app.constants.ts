export const BOARD_SIZE = 18;

export const CONTROLS = {
  LEFT: 2,
  UP: 8,
  RIGHT: 4,
  DOWN: 16
};

export const COLORS = {
  GAME_OVER: '#D24D57',
  FRUIT: '#EC644B',
  HEAD: '#336E7B',
  BODY: '#C8F7C5',
  BOARD: '#86B5BD',
  OBSTACLE: '#383522'
};

export const GAME_MODES = {
  classic: 'Classic',
  no_walls: 'No Walls',
  obstacles: 'Obstacles'
};
