import { Component } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  ModalController } from '@ionic/angular';

// Modals
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';
// Call notifications test by Popover and Custom Component.
import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { Socket } from 'ng-socket-io';

import { InitService } from './services/init/init.service';
import { MainService } from 'src/app/infrastructure/api.service';
import { environment } from 'src/environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage {
  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/img/ionic4-Start-Theme-cover.jpg';
  allTrains:[]=[];
  allLiveTrains:[]=[];
  trainSelected: any;
  validateStatus:boolean;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private socket: Socket,
    private geolocation: Geolocation,
    private _InitService: InitService,
    private _MainServicesService:MainService
  ) {
this._InitService.getAllTrains().subscribe(data=>{
  this.allTrains = data.data;
});
    this.socket.connect();
    this.socket.on('realTimeTable', (data) => {
    console.log(data);
    this.allLiveTrains = data;
    });
  }
  setLanguage(event) {
    let me=this;
    console.log(event.target.value);
    this.trainSelected=event.target.value;
    this._MainServicesService.setId(event.target.value);
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      console.log("getting data..!");
     // data can be a set of coordinates, or an error (if an error occurred).
     // data.coords.latitude
     // data.coords.longitude
if(data.coords && data.coords.longitude){
  let obj ={"id": event.target.value,"point": { "lon": data.coords.longitude, "lat":data.coords.latitude }};
  this._InitService.validatePlace(obj).subscribe(data=>{
    console.log(data);
    this.validateStatus=data.data.validateStatus;
    if(data.data.validateStatus){
      this.geolocation.getCurrentPosition().then((resp1) => {
        // resp.coords.latitude
        // resp.coords.longitude
        let newObj={
          "firstLat": 0,
          "firstLng": 0,
          "firstTime":new Date().getTime(),
          "secondLat":0,
          "secondLng":0,
          "secondTime":new Date().getTime(),
          "id": event.target.value
         }
         console.log("innn");
        if(resp1.coords && resp1.coords.longitude){
          newObj={
            "firstLat": resp1.coords.latitude,
            "firstLng": resp1.coords.longitude,
            "firstTime":new Date().getTime(),
            "secondLat":resp1.coords.latitude,
            "secondLng":resp1.coords.longitude,
            "secondTime":new Date().getTime(),
            "id": event.target.value
           }
           console.log("data seted 1");
         console.log(newObj);
        }
       
      setTimeout( () => {
        // somecode
        this.geolocation.getCurrentPosition().then((resp2) => {
          // resp.coords.latitude
          // resp.coords.longitude
          if(resp2.coords && resp2.coords.longitude){
            newObj.secondLat =resp2.coords.latitude;
            newObj.secondLng =resp2.coords.longitude;
            newObj.secondTime = new Date().getTime();
            console.log("data seted 2");
            console.log(newObj);
            this.socket.emit('realtime-status-data', newObj);
            console.log("data sent");
          }

        }).catch((error) => {
           console.log('Error getting location', error);
         });
         
   }, environment.wait_time_for_two_locations);      
       }).catch((error) => {
         console.log('Error getting location', error);
       });
       


    }
});
}
    });

  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Current Train',
      message: 'Type your Address.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.yourLocation = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

}
