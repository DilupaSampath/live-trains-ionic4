
// import common service

import { Observable } from 'rxjs';
import { MainService } from 'src/app/infrastructure/api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.localStorage.getItem('userId');

  getAllTrains(): Observable<any> {
    return this._apiService.get(`train/getAll`);
  }

  getUsersBestScore(): Observable<any> {
    return this._apiService.get(`api/feedback/getOne/${this.loggedUser}`);
  }
  

  validatePlace(obj): Observable<any> {
    return this._apiService.post(`googleApi/validatePoint`, obj);
  }
  postObjNearByPlaceMapRequest(obj): Observable<any> {
    return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  }

  validatePoint(obj): Observable<any> {
    return this._apiService.post(`googleApi/validatePoint`, obj);
  }

  putObj(obj): Observable<any> {
    return this._apiService.patch(
      `api/feedback//update/${this.loggedUser}`,
      obj
    );
  }

  deleteObj(id): Observable<any> {
    return this._apiService.delete(`api/feedback/remove/${id}`);
  }
}
