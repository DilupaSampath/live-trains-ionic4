import { Component, OnInit } from '@angular/core';
import { InitService } from './services/init/init.service';
import { MainService } from 'src/app/infrastructure/api.service';
import { ModalController } from '@ionic/angular';
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {
  allTrains:[]=[];
  searchKey = '';
  constructor(
    private _InitService: InitService,
    public modalCtrl: ModalController,
  ) { 
    // this.getAllTrains();
  }

  ngOnInit() {
    this.getAllTrains();
  }
  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }
getAllTrains(){
  this._InitService.getAllSchedues().subscribe(data=>{
    console.log(data.data);
    this.allTrains = data.data;
  });
}

}
