import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { JourneyPage } from './journey.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { InitService } from './services/init/init.service';
declare var google;
const routes: Routes = [
  {
    path: '',
    component: JourneyPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  declarations: [JourneyPage],
  providers:[InitService,Geolocation]
})
export class JourneyPageModule {}
