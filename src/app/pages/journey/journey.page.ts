import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { InitService } from './services/init/init.service';
import { AlertController } from '@ionic/angular';
declare var google;
@Component({
  selector: 'app-journey',
  templateUrl: './journey.page.html',
  styleUrls: ['./journey.page.scss'],
})
export class JourneyPage implements OnInit , AfterViewInit {
  map:any;
  @ViewChild('map') mapNativeElement: ElementRef;
  @ViewChild('pleaseConnect') pleaseConnect: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  directionForm: FormGroup;
  GoogleAutocomplete:any;
  autocomplete:any;
  autocompleteItems:any
  nearByData:any;
  
  constructor(public alertController: AlertController,private fb: FormBuilder,private _InitService: InitService,private ngZOne:NgZone) {
    this.createDirectionForm();
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
this.autocomplete = { input: '' };
this.autocompleteItems = [];

}
async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'Set this location as..',
      buttons: ['Destination', 'Source','Cancel']
    });

    await alert.present();
  }

addEventListnerTomarker(marker){
  google.maps.event.addListener(marker, 'click', () => {
    //Call run function to set the data within angular zone to trigger change detection.
    this.ngZOne.run(()=>{
      this.presentAlertMultipleButtons();
console.log(marker.position.lat());
console.log(marker.position.lng());
console.log(marker);
    });
  })
}
  ngOnInit() {
    this.loadMap();
  }

  ionViewDidLoad(){


    // });

}


addMarker(lat: number, lng: number): void {

  let latLng = new google.maps.LatLng(lat, lng);

  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: latLng
  });
  this.addEventListnerTomarker(marker);
  marker.setMap(this.map);
  // this.markers.push(marker);  

}
// addMarkersToMap(train,tite,status) {
//   let museumMarker = new google.maps.Marker();
//   if(status){
//     const position = new google.maps.LatLng(train[1], train[0]);
//      museumMarker = new google.maps.Marker({ position, title: tite,label:tite});
    
//   }else{
//     const position = new google.maps.LatLng(train[1], train[0]);
//      museumMarker = new google.maps.Marker({ position});
    
//   }
  
//   museumMarker.setMap(this.map);

// }
  /**
   * Set directions
   * @param start
   * @param end
   */
  async setPath(start, end) {
    await this.loadMap();
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(this.map);

    directionsService.route(
      {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode["TRANSIT"],
        transitOptions: {
          modes: ["TRAIN"],
          routingPreference: "FEWER_TRANSFERS"
        },
        avoidHighways: true
      },
      (res, status) => {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(res);
        } else {
          console.warn(status);
        }
      }
    );
  }
  createDirectionForm() {
    this.directionForm = this.fb.group({
      source: ['', Validators.required],
      destination: ['', Validators.required]
    });
  }
  async loadMap() {
    let mapOptions = {
      center: new google.maps.LatLng(7.8731,80.7718),
      zoom: 7.4,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
  this.map = new google.maps.Map(this.mapNativeElement.nativeElement, mapOptions);
  }

  ngAfterViewInit(): void {
    // this.loadMap();
    this.load();
  }

  calculateAndDisplayRoute(formValues) {
    this.setPath(formValues.source,formValues.destination);
  }

  load(){

    if(this.nearByData){
        return Promise.resolve(this.nearByData);
    }

    return new Promise(resolve => {

        this._InitService.postObjNearByPlaceMapRequest({
          "lat":"6.9332",
          "lang":"79.8773"
         }).subscribe(data => {
console.log(data);
            this.nearByData = this.applyHaversine(data.data.result);

            this.nearByData.sort((locationA, locationB) => {
                return locationA.distance - locationB.distance;
            });
            this.nearByData.forEach(element => {
              this.addMarker(element.latitude,element.longitude);              
            });

            resolve(this.nearByData);
        });

    });

}
applyHaversine(locations){

  let usersLocation = {
      lat: 40.713744, 
      lng: -74.009056
  };

  locations.map((location) => {

      let placeLocation = {
          lat: location.latitude,
          lng: location.longitude
      };

      location.distance = this.getDistanceBetweenPoints(
          usersLocation,
          placeLocation,
          'miles'
      ).toFixed(2);
  });

  return locations;
}
getDistanceBetweenPoints(start, end, units){

  let earthRadius = {
      miles: 3958.8,
      km: 6371
  };

  let R = earthRadius[units || 'miles'];
  let lat1 = start.lat;
  let lon1 = start.lng;
  let lat2 = end.lat;
  let lon2 = end.lng;

  let dLat = this.toRad((lat2 - lat1));
  let dLon = this.toRad((lon2 - lon1));
  let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
  Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
  Math.sin(dLon / 2) *
  Math.sin(dLon / 2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = R * c;

  return d;

}
toRad(x){
  return x * Math.PI / 180;
}
}