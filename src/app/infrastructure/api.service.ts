import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { environment } from '../../environments/environment';

/**
 * import services
 * */

@Injectable({
  providedIn: 'root'
})
export class MainService {
private currentTrainId:any;
  constructor(private http: Http) {}

  // Setting Headers for API Request
  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    };

    // if (this.jwtService.getToken()) {
    //   headersConfig['Authorization'] = `Bearer ${this.jwtService.getToken()}`;
    // }
    return new Headers(headersConfig);
  }
  setId(id:any){
    this.currentTrainId=id;
      }
  getId(){
        return this.currentTrainId;
        }
  // Perform a GET Request
  get(path: string): Observable<any> {
    return this.http
      .get(`${environment.api_url}${path}`, { headers: this.setHeaders() })
      .pipe(
        catchError(error => {
          throw error.json();
        }),
        map((res: Response) => res.json())
      );
  }
  // Perform a GET Request
  getFromOtherServer(path: string): Observable<any> {
    return this.http.get(`${path}`, { headers: this.setHeaders() }).pipe(
      catchError(error => {
        throw error.json();
      }),
      map((res: Response) => res.json())
    );
  }

  // Perform a PUT Request
  put(path: string, body): Observable<any> {
    return this.http
      .put(
        `${environment.api_url}${path}`,
        JSON.stringify(this.filterInputs(body)),
        {
          headers: this.setHeaders()
        }
      )
      .pipe(
        catchError(error => {
          throw error.json();
        }),
        map((res: Response) => res.json())
      );
  }

  // Perform a PATCH Request
  patch(path: string, body): Observable<any> {
    return this.http
      .patch(
        `${environment.api_url}${path}`,
        JSON.stringify(this.filterInputs(body)),
        {
          headers: this.setHeaders()
        }
      )
      .pipe(
        catchError(error => {
          throw error.json();
        }),
        map((res: Response) => res.json())
      );
  }

  // Perform POST Request
  post(path, body): Observable<any> {
    return this.http
      .post(
        `${environment.api_url}${path}`,
        JSON.stringify(this.filterInputs(body)),
        {
          headers: this.setHeaders()
        }
      )
      .pipe(
        catchError(error => {
          throw error.json();
        }),
        map((res: Response) => res.json())
      );
  }

  // Perform Delete Request
  delete(path): Observable<any> {
    return this.http
      .delete(`${environment.api_url}${path}`, { headers: this.setHeaders() })
      .pipe(
        catchError(error => {
          throw error.json();
        }),
        map((res: Response) => res.json())
      );
  }

  // filter the input values
  filterInputs(input: any) {
    const tempObj = {};
    Object.keys(input).forEach(key => {
      if (
        input[key] !== undefined &&
        input[key] !== null &&
        input[key] !== ''
      ) {
        tempObj[key] = input[key];
      }
    });

    return tempObj;
  }
}
